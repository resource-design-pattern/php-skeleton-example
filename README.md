# Resource Design Pattern - Php Skeleton Example

This repository was created using the [Skeleton Generator Tool](https://gitlab.com/resource-design-pattern/skeleton-generator) to create a
Php Skeleton that implements the Resource Design Pattern.

Anyone looking into the Resources folder can immediately see what Resources and what actions are available for each one 
without the need to consult any documentation. This is the skeleton acting as documentation in our projects.

src/Resources/  

├── Carts  
│   ├── New  
│   ├── Modify  
│   ├── View  
│   └── Remove  
├── Categories  
│   ├── New  
│   ├── Modify  
│   ├── View  
│   └── Remove  
├── Checkout  
│   ├── New  
│   ├── Modify  
│   ├── View  
│   └── Remove  
├── Clients  
│   ├── New  
│   ├── Modify  
│   ├── View  
│   └── Remove  
├── Products  
│   ├── New    
│   ├── Modify    
│   ├── View    
│   └── Remove    
└── ...  


The above skeleton by design will easily allow our code to follow the [S.O.L.I.D Principles](https://scotch.io/bar-talk/s-o-l-i-d-the-first-five-principles-of-object-oriented-design).


# Resource Design Pattern Main Goals

* Make easy to write SOLID and Clean Code.
* Easy On-boarding for new developers on the project.
* To act as live documentation.
